/* calculate sha1sum of dict.info part from torrent seed file */
#ifndef INFOHASH_INCLUDED
#define INFOHASH_INCLUDED
extern int info_sha1(char *file, char* result);
#endif
