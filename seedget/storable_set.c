#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include <cii/assert.h>
#include <cii/set.h>

#include "hashstr.h"
#include "storable_set.h"

static void save_key(const void *key, void *cl) {
	int f = *(int *)cl;
	int nbytes = strlen(key)+1; /* include null */
	int error = 0;

	if (nbytes != write(f, (char *)key, nbytes)) {
		perror("cannot write to file");
		error = 1;
	}

	if (error) {
		exit(1);
	}
}

/* store Set into a file
 * return:
 *   1  Success
 *   0  Failure
 */
int store(Set_T set, const char *s) {
	int fd;
	fd = open(s, O_WRONLY | O_CREAT | O_TRUNC, (mode_t)0644);
	if ( fd < 0 ) {
		perror("cannot open file to write");
		return 0;
	}

	Set_map(set, save_key, (void *)&fd);
	close(fd);
	return 1;
}


/* retrieve history data from file into memory
 * param:
 *   s: string: file name
 * return: Set_T
 */
Set_T retrieve(const char *s) {
	Set_T set;
	set = Set_new(0, (int (*)(const void *, const void *))strcmp, hashstr);

	if ( 0 != access(s, R_OK)) /* file exist and readable*/
		return set;


	int fd;
	fd = open(s, O_RDONLY);
	assert(fd > 0);

	struct stat sb;
	assert(fstat(fd, &sb) == 0);

	size_t len = sb.st_size;
	if ( len == 0 ) {
		return set;
	}

	char *map;
	map = mmap(0, len, PROT_READ, MAP_SHARED, fd, 0);

	/* map file into memory */
	if (map == MAP_FAILED) {
		close(fd);
		perror("Error mmaping the file");
		exit(1);
	}

	/* allocate a memory area to hold keys */
	char *nmap = malloc(len);
	if (nmap == NULL) {
		perror("Error alloc memory");
		exit(1);
	}
	memcpy(nmap, map, len);
	close(fd);
	
	char *p;
	int in = 0;
	for (p = nmap; p - nmap < len; p++) {
		if ( '\0' == *p ) {
			in = 0;
		} else {
			if (! in) {
				char* np = p;
				assert(np);
				Set_put(set, np);
				in = 1;
			}
		}

	}
	return set;
}


/* Unit tests */
#include "cutest/CuTest.h"

#define TESTCACHEFILE "/tmp/testcache.dat"
#define ENTRY1 "aba721d0be2b288d4b2b7e462cd1e42106f4ecd3"
#define ENTRY2 "df7f0a7f2d1df1c608b6a407f70325dff12e43dd"

static void print_set_cb(const void *key, void *cl) {
	fprintf(stderr, "key: %s\n", (char*)key);
}

static void print_set(Set_T set) {
	assert(set);
	Set_map(set, print_set_cb, NULL);
}

void TestStorable_store(CuTest *tc) {
	Set_T set = Set_new(0, (int (*)(const void *, const void *))strcmp, hashstr);
	Set_put(set, ENTRY1);
	Set_put(set, ENTRY2);
	int result = store(set, TESTCACHEFILE);
        CuAssertIntEquals(tc, 1, result);
}


void TestStorable_retrieve(CuTest *tc) {
	Set_T set;
	set = retrieve(TESTCACHEFILE);
//	print_set(set);
	CuAssertIntEquals(tc, 2, Set_length(set));
	CuAssertIntEquals(tc, 1, Set_member(set, ENTRY1));
	CuAssertIntEquals(tc, 1, Set_member(set, ENTRY2));
	remove(TESTCACHEFILE);
}



#undef TESTCACHEFILE
#undef ENTRY1
#undef ENTRY2

