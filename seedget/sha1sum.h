/* sha1sum.c - print SHA-1 Message-Digest Algorithm 
 * Copyright (C) 1998, 1999, 2000, 2001 Free Software Foundation, Inc.
 * Copyright (C) 2004 g10 Code GmbH
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* SHA-1 coden take from gnupg 1.3.92.  */

#ifndef SHA1SUM_INCLUDED
#define SHA1SUM_INCLUDED

typedef struct SHA1_CONTEXT_T SHA1_CONTEXT;
typedef unsigned int u32;

struct SHA1_CONTEXT_T {
    u32  h0,h1,h2,h3,h4;
    u32  nblocks;
    unsigned char buf[64];
    int  count;
};


extern void sha1_init(SHA1_CONTEXT *hd);

/* Update the message digest with the contents
 * of INBUF with length INLEN.
 */
extern void sha1_write( SHA1_CONTEXT *hd, unsigned char *inbuf, size_t inlen);


/* The routine final terminates the computation and
 * returns the digest.
 * The handle is prepared for a new cycle, but adding bytes to the
 * handle will the destroy the returned buffer.
 * Returns: 20 bytes representing the digest.
 */
extern void sha1_final(SHA1_CONTEXT *hd);

#endif
