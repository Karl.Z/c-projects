/* calculate sha1sum of dict.info part from torrent seed file */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <cii/mem.h>
#include <cii/assert.h>
#include <cii/except.h>

#include "debug.h"
#include "sha1sum.h"

static char *
skip_number(char *q) {
	if (*q == 'i') q++;
	else return NULL;

	for (;;) {
		if (*q >= '0' && *q <='9') q++;
		else if (*q == 'e') break;
		else return NULL;
	}
	return ++q;
}

static char *
skip_string(char *p) {
	if (*p < '0' || *p > '9')
		return NULL;

	int sum = 0;
	for (; *p >= '0' && *p <= '9'; p++) {
		sum = sum * 10 + (*p - '0');
	}

	if (*p == ':') return p+1+sum;
	else return NULL;
}

static char *
pass_string(char **q) {
	assert(*q);
	assert(q);
	char *p = *q;
	if ('\0' == *p) return NULL;
	char buf[6];
	int i=0;
	for (; i<sizeof(buf); i++, p++) {
		if (*p >= '0' && *p <= '9') {
			buf[i] = *p;
		} else if (*p == ':') {
			buf[i] = '\0';
			p++;
			break;
		} else {
			return NULL;
		}
	}
	if (i>5) return NULL;
	int len = atoi(buf);
//	dputs("got string length: %d", len);

	char *s = ALLOC(len+1);
	assert(s);
	for ( i=0; i < len; i++) {
		s[i] = *p++;
	}
	s[i] = '\0';
	*q = p;

	return s;
}

/* find info part offset from give string b */
/* fbegin normally should be beginning of a torrent file */
/* return 0 when cannot find it, else info part size_t */
static size_t
find_info(char *fbegin, ssize_t size, 
                        char** begin, char** end) {
	char *info_begin = NULL;
	char *info_end = NULL;
	char *b = fbegin;
	int e = 0;
	while (b - fbegin < size) {
		char *key;
//		dputs("found '%c' at file offset 0x%x", *b, b-fbegin);
		switch (*b) {
			case 'd': /* dict */
			case 'l': /* list */
				++b;
				if (e) e++;
				continue;
			case 'i': /* number */
				b = skip_number(b);
				continue;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				if (e) 
					b = skip_string(b);
				else {
					key = pass_string(&b);
//					dputs("got key: %s", key);
					if (NULL == key) break;
					if (strcmp(key, "info") == 0) {
						/* found info part */
						dputs("found info at offset 0x%x", b-fbegin);
						info_begin = b;
						++e;
					} 
					FREE(key);
				}
				continue;
			case 'e':
				++b;
				if (e) e--;
				if (e == 1 && info_begin) {
					info_end = b;
					dputs("found end of info part at file offset 0x%x", info_end - fbegin);
					break;
				}
				continue;
			default:
				dputs("unknown character: %c at file offset 0x%x", *b, b - fbegin);
				break;
		}
		break;
	}

	
	if (info_end && info_begin) {
		*begin = info_begin;
		*end = info_end;
		return (info_end - info_begin);
	} else {
		return 0;
	}
}

extern int
info_sha1(char *file, char* result) {
	assert(file);
	int fd = open(file, O_RDONLY);
	assert (fd>0);

	struct stat sb;
	fstat(fd, &sb);

	dputs("file size: %d", (int)sb.st_size);
	assert(sb.st_size > 0);

	char* fbegin = mmap(0, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
	assert(fbegin);

	char* info_begin = NULL; /* store info part beginning address */
	char* info_end = NULL;

	size_t sz = find_info(fbegin, sb.st_size, &info_begin, &info_end);

	if ( 0 >= sz) {
		dputs("cannot find info part in file");
		close(fd);
		return 0;
	}

	/* calculate sha1 */
	SHA1_CONTEXT ctx;
	sha1_init(&ctx);
	sha1_write(&ctx, (unsigned char *)info_begin, sz);
	sha1_final(&ctx);
	int i;
	char buf[3] = {'\0'};
	for (i=0; i < 20; i++) {
		snprintf (buf, sizeof(buf), "%02x", ctx.buf[i]);
		strcat(result, buf);
	}

	close(fd);
	return 1;
}

